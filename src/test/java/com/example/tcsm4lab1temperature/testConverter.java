package com.example.tcsm4lab1temperature;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

public class testConverter {
    @Test
    public void testCToF() throws Exception{
        Assertions.assertEquals(212, Converter.celsiusToFahrenheit(100));
    }

}
