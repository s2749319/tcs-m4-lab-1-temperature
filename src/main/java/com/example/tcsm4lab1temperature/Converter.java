package com.example.tcsm4lab1temperature;

public class Converter {
    public static double celsiusToFahrenheit(double celsius) {
        return ((celsius * 1.8) + 32);
    }

    public static double fahrenheitToCelsius(double fahrenheit) {
        return ((fahrenheit - 32)/1.8);
    }
}
